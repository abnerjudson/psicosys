# Instruções

Git global setup
<pre><code>
git config --global user.name "Abner Judson Torres"
git config --global user.email "abnerjudson@gmail.com"
</pre></code>

Create a new repository
<pre><code>
git clone https://gitlab.com/abnerjudson/psicosys.git
cd psicosys
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
</pre></code>

Existing folder
<pre><code>
cd existing_folder
git init
git remote add origin https://gitlab.com/abnerjudson/psicosys.git
git add .
git commit -m "Initial commit"
git push -u origin master
</pre></code>

Existing Git repository
<pre><code>
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/abnerjudson/psicosys.git
git push -u origin --all
git push -u origin --tags
</pre></code>
