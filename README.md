# PsicoSys

É um Sistema de Registro de Pacientes de Consultório Psicológico que está sendo prototipado por alunos da Universidade Federal de São Paulo (UNIFESP), do curso de Tecnologia em Informática em Saúde, durante as disciplinas de Engenharia de Software e Programação Web. As funcionalidades planejadas são:


* Controle de agenda: o usuário poderá visualizar, agendar, remarcar e cancelar
horários de pacientes.

* Controle de registros: o usuário poderá anotar e organizar as consultas de seus
pacientes.

* Controle de finanças: o usuário poderá organizar as finanças.

* Inclusão de relatórios: inclusão de novos registros e relatórios das consultas, por
meio de campo de texto ou de anexo de foto das anotações feitas em papel.

* Emissão de relatórios: o usuário poderá imprimir relatórios de anamnese,
consultas dos pacientes, finanças do consultório.

* Emissão de documentos diversos: o usuário poderá gerar atestados e recibos
para seus pacientes.

* Emissão de boleto: o usuário poderá gerar boletos aos seus pacientes.

* Notificações: envio de horário de consultas via e-mail.

* Mobilidade: o usuário poderá acessar o sistema via smartphone e tablet.


